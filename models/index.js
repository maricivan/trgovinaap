var Sequelize = require('sequelize');

var sequelize = new Sequelize('trgovinadb', 'root', 'SQLmySQL2!');

var models = [
    'Klijent',
    'Kupovina'
];

models.forEach(function(model) {
    module.exports[model] = sequelize.import(__dirname + '/' + model);
});

// describe relationships
(function(m) {
    m.Kupovina.belongsTo(m.Klijent);
    m.Klijent.hasMany(m.Kupovina);
})(module.exports);

// export connection
module.exports.sequelize = sequelize;

sequelize.sync();