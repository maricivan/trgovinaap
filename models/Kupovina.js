var Sequelize = require('sequelize');

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Kupovina', {
        proizvod: {type: DataTypes.STRING, allowNull: false},
        kolicina: {type: DataTypes.INTEGER, allowNull: false},
        cijena: {type: DataTypes.DECIMAL, allowNull: false},
        ukupno: {type: DataTypes.DECIMAL, allowNull: false},
        napomena: {type: DataTypes.STRING, allowNull: false}
    });
};