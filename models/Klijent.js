var Sequelize = require('sequelize');

module.exports = function(sequelize, DataTypes) {
    return sequelize.define('Klijent', {
        ime: {type: DataTypes.STRING, allowNull: false},
        prezime: {type: DataTypes.STRING, allowNull: false},
        adresa: {type: DataTypes.STRING, allowNull: false},
        kontaktBroj: {type: DataTypes.STRING, allowNull: false},
        napomena: {type: DataTypes.STRING, allowNull: true}
    });
};