Preduvjeti za pokretanje aplikacije:
==============

- U mysql-u kreirati schemu pod nazivom **'trgovinadb'**
- U file-u models/index.js u 3. liniji koda promijeniti password koji odgovara Vašem mysql passwordu i eventualno promijeniti usera.
- Instalirati [node.js](https://nodejs.org)
- Otvoriti cmd u istom folderu gdje je spremljen projekt
- **npm install**
- **npm install bower -g**
- **bower install**
- **npm start** (pokretanje servera na portu 5000), odnosno **gulp serve** (za pokretanje servera na portu 8282 uz jshint i jscs provjere
   te unos referenci na zavisne fileove u views/index.ejs)