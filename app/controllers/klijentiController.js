(function () {

    var klijentiController = function ($scope, store, auth, $log, $location, trgovinaFactory) {

        $scope.klijenti = [];
        $scope.trazi = [];
        $scope.traziPo = 'ime';
        $scope.trenutnaStr = 0;
        $scope.strVelicina = 9;
        $scope.auth = auth;
        $scope.brojStranica = function () {
            return Math.ceil($scope.klijenti.length / $scope.strVelicina);
        };

        trgovinaFactory.getKlijenti()
            .success(function (klijenti) {
                $scope.klijenti = klijenti;
            })
            .error(function (data, status, headers, config) {
                $log.log(data.error + ' ' + status);
            });

        $scope.brisi = function (klijentId) {
            if (auth.profile.roles[0] === 'admin') {
                trgovinaFactory.deleteKlijent(klijentId)
                    .success(function (klijent) {
                        for (var i = 0; i < $scope.klijenti.length; i++) {
                            if ($scope.klijenti[i].id === klijentId) {
                                $scope.klijenti.splice(i, 1);
                                break;
                            }
                        }
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                    });
            }
        };

        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };

        $scope.classPanel = 'panel panel-default';

        $scope.addClass = function () {
            this.classPanel = 'panel panel-success';
        };
        $scope.removeClass = function () {
            this.classPanel = 'panel panel-default';
        };

        $scope.prethodna = function () {
            if ($scope.trenutnaStr > 0) {
                $scope.trenutnaStr = $scope.trenutnaStr - 1;
            }
        };

        $scope.sljedeca = function () {
            var brStr = $scope.brojStranica;
            if ($scope.trenutnaStr < brStr() - 1) {
                $scope.trenutnaStr = $scope.trenutnaStr + 1;
            }
        };

    };

    klijentiController.$inject = ['$scope', 'store', 'auth', '$log', '$location', 'trgovinaFactory'];

    angular.module('trgApp').controller('klijentiController', klijentiController);

} ());