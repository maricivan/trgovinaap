(function () {

    var noviklijentController = function ($scope, auth, $log, trgovinaFactory) {

        $scope.klijenti = {};
        $scope.dodan = null;
        $scope.nedodan = null;

        $scope.addKlijent = function () {
            if (auth.profile.roles[0] === 'admin') {
                trgovinaFactory.postKlijent($scope.klijenti)
                    .success(function (res) {
                        console.log(res);
                        $scope.dodan = true;
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                        $scope.nedodan = true;
                    });
                $scope.klijenti = '';
            }
        };

    };

    noviklijentController.$inject = ['$scope', 'auth', '$log', 'trgovinaFactory'];

    angular.module('trgApp').controller('noviklijentController', noviklijentController);

} ());