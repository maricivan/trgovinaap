(function () {

    var kupovineAllController = function ($scope, store, auth, $log, $location, trgovinaFactory) {

        $scope.kupovine = [];
        $scope.ukupno = null;
        $scope.auth = auth;

        trgovinaFactory.getAllKupovine()
            .success(function (kupovine) {
                $scope.kupovine = kupovine;
                for (var i = 0; i < kupovine.length; i += 1) {
                    $scope.ukupno = $scope.ukupno + kupovine[i].ukupno;
                }
            })
            .error(function (data, status, headers, config) {
                $log.log(data.error + ' ' + status);
            });

    };

    kupovineAllController.$inject = ['$scope', 'store', 'auth', '$log', '$location', 'trgovinaFactory'];

    angular.module('trgApp').controller('kupovineAllController', kupovineAllController);

} ());