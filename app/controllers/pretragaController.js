(function () {

    var pretragaController = function ($scope, store, auth, $log, trgovinaFactory) {

        $scope.klijenti = [];
        $scope.klijenti2 = [];
        $scope.klijenti3 = [];
        $scope.klijenti4 = [];
        $scope.auth = auth;

        $scope.getIme = function () {
            if ($scope.kli.ime !== '') {
                var klijent = this.kli;

                trgovinaFactory.findIme(klijent)
                    .success(function (res) {
                        console.log('Succes!');
                        $scope.klijenti = res;
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                    });
            } else { console.log('Prazan unos'); }
        };

        $scope.getPrezime = function () {
            if ($scope.kli.prezime !== '') {
                var klijent = this.kli;

                trgovinaFactory.findPrezime(klijent)
                    .success(function (res) {
                        console.log('Succes!');
                        $scope.klijenti2 = res;
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                    });
            } else { console.log('Prazan unos'); }
        };

        $scope.getBroj = function () {
            if ($scope.kli.kontaktBroj !== '') {
                var klijent = this.kli;

                trgovinaFactory.findBroj(klijent)
                    .success(function (res) {
                        console.log('Succes!');
                        $scope.klijenti3 = res;
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                    });
            } else { console.log('Prazan unos'); }
        };

        $scope.getAdresa = function () {
            if ($scope.kli.adresa !== '') {
                var klijent = this.kli;

                trgovinaFactory.findAdresa(klijent)
                    .success(function (res) {
                        console.log('Succes!');
                        $scope.klijenti4 = res;
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                    });
            } else { console.log('Prazan unos'); }
        };

        $scope.classPanel = 'panel panel-default';

        $scope.addClass = function () {
            this.classPanel = 'panel panel-success';
        };
        $scope.removeClass = function () {
            this.classPanel = 'panel panel-default';
        };
    };

    pretragaController.$inject = ['$scope', 'store', 'auth', '$log', 'trgovinaFactory'];

    angular.module('trgApp').controller('pretragaController', pretragaController);

} ());