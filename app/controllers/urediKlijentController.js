(function () {

    var urediKlijentController = function ($scope, auth, $log, $routeParams, trgovinaFactory) {

        var klijentId = $routeParams.klijentId;
        $scope.klijent = null;
        $scope.uredjen = null;
        $scope.neuredjen = null;

        trgovinaFactory.getKlijent(klijentId)
            .success(function (klijent) {
                $scope.klijent = klijent;
            })
            .error(function (data, status, headers, config) {
                $log.log(data.error + ' ' + status);
            });

        $scope.uredi = function () {
            if (auth.profile.roles[0] === 'admin') {
                trgovinaFactory.putKlijent(klijentId, $scope.klijent)
                    .success(function (klijent) {
                        $scope.klijent = klijent;
                        $scope.uredjen = true;
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                        $scope.neuredjen = true;
                    });
            }
        };

    };

    urediKlijentController.$inject = ['$scope', 'auth', '$log', '$routeParams', 'trgovinaFactory'];

    angular.module('trgApp').controller('urediKlijentController', urediKlijentController);

} ());