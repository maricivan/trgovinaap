(function () {

    var authController = function ($scope, store, auth, $location) {

        $scope.auth = auth;
        $scope.login = login;
        $scope.logout = logout;

        function login() {
            //opcije npr slika => {}
            auth.signin({
                icon: '../favicon/shopping-cart-grey-md.png'
            }, function (profile, token) {
                store.set('profile', profile);
                store.set('id_token', token);
                $location.path('/');
                console.log($scope.auth.profile.roles[0]);
                var korisnik = $scope.auth.profile.roles[0];
                if (korisnik === 'admin') {
                    $scope.admin = true;
                } else {
                    $scope.admin = false;
                }
                console.log($scope.admin);
            }, function (error) {
                console.log(error);
            });
        }

        // $scope.logout = function () {
        //     auth.signout();
        //     store.remove('profile');
        //     store.remove('token');
        //     $location.path('/login');
        // };

        function logout() {

            store.remove('profile');
            store.remove('id_token');
            store.remove('token');
            auth.signout();
            $location.path('/login');

        }

    };

    authController.$inject = ['$scope', 'store', 'auth', '$location'];

    angular.module('trgApp').controller('authController', authController);

} ());