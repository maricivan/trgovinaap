(function () {

    var kupovineController = function ($scope, $location, store, auth, $log, $routeParams, trgovinaFactory, ukupnaCijena) {

        var klijentId = $routeParams.klijentId;
        var kupovinaId = $routeParams.kupovinaId;
        $scope.kupovine = null;
        $scope.kupovina = null;
        $scope.klijent = null;
        $scope.dodan = null;
        $scope.nedodan = null;
        $scope.uredjen = null;
        $scope.neuredjen = null;
        $scope.ukupno = null;
        $scope.auth = auth;
        $scope.novaKupovina = {};

        $scope.labels = [];
        $scope.data = [];

        trgovinaFactory.getKupovine(klijentId)
            .success(function (kupovina) {
                $scope.kupovine = kupovina;
                for (var i = 0; i < kupovina.length; i += 1) {
                    $scope.ukupno = $scope.ukupno + kupovina[i].ukupno;
                    $scope.labels.push(kupovina[i].proizvod);
                    $scope.data.push(kupovina[i].ukupno);
                }
            })
            .error(function (data, status, headers, config) {
                $log.log(data.error + ' ' + status);
            });

        trgovinaFactory.getKlijent(klijentId)
            .success(function (klijent) {
                $scope.klijent = klijent;
            })
            .error(function (data, status, headers, config) {
                $log.log(data.error + ' ' + status);
            });

        $scope.addKupovina = function () {
            if (auth.profile.roles[0] === 'admin') {
                console.log(ukupnaCijena);
                $scope.novaKupovina.ukupno = ukupnaCijena.racun($scope.novaKupovina.cijena, $scope.novaKupovina.kolicina);
                trgovinaFactory.postKupovine(klijentId, $scope.novaKupovina)
                    .success(function (res) {
                        console.log(res);
                        $scope.novaKupovina.proizvod = '';
                        $scope.novaKupovina.kolicina = '';
                        $scope.novaKupovina.cijena = '';
                        $scope.novaKupovina.napomena = '';
                        $scope.dodan = true;
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                        $scope.nedodan = true;
                    });
            }
        };

        trgovinaFactory.getKupovinu(kupovinaId)
            .success(function (kupovina) {
                $scope.kupovina = kupovina;
            })
            .error(function (data, status, headers, config) {
                $log.log(data.error + ' ' + status);
            });

        $scope.uredi = function () {
            if (auth.profile.roles[0] === 'admin') {
                $scope.kupovina.ukupno = ukupnaCijena.racun($scope.kupovina.cijena, $scope.kupovina.kolicina);
                trgovinaFactory.putKupovina(kupovinaId, $scope.kupovina)
                    .success(function (kupovina) {
                        $scope.kupovina = kupovina;
                        $scope.uredjen = true;
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                        $scope.neuredjen = true;
                    });
                // setTimeout(function ($location) {
                //     $location.path('/kupovine/' + $scope.kupovina.klijentId);
                // }, 2000);
            }
        };

        $scope.brisi = function (kupovinaId) {
            if (auth.profile.roles[0] === 'admin') {
                trgovinaFactory.deleteKupovina(kupovinaId)
                    .success(function (kupovina) {
                        for (var i = 0; i < $scope.kupovine.length; i++) {
                            if ($scope.kupovine[i].id === kupovinaId) {
                                $scope.kupovine.splice(i, 1);
                                break;
                            }
                        }
                    })
                    .error(function (data, status, headers, config) {
                        $log.log(data.error + ' ' + status);
                    });
            }
        };

    };

    kupovineController.$inject = ['$scope', '$location', 'store', 'auth', '$log', '$routeParams', 'trgovinaFactory', 'ukupnaCijena'];

    angular.module('trgApp').controller('kupovineController', kupovineController);

} ());