(function () {

    var tecajController = function ($scope, store, auth, $log, trgovinaFactory) {

        $scope.tecaj = null;
        $scope.novac = null;
        $scope.convert = null;
        $scope.para1 = 'USD';
        $scope.para2 = 1;
        $scope.para3 = 'GBP';
        $scope.spinner = false;

        trgovinaFactory.getTecaj()
            .success(function (res) {
                $scope.tecaj = res;
            })
            .error(function (data, status, headers, config) {
                $log.log(data.error + ' ' + status);
            });

        $scope.convertValue = function () {
            var para1 = this.para1;
            //var para2 = this.para2;
            var para3 = this.para3;
            $scope.spinner = true;

            trgovinaFactory.getConvert(para1, para3)
                .success(function (res) {
                    // console.log('Succes!');
                    $scope.spinner = false;
                    $scope.convert = res;
                    // if (res) {
                    //     $scope.para1 = '';
                    //     $scope.para2 = '';
                    //     $scope.para3 = '';
                    // }
                })
                .error(function (data, status, headers, config) {
                    $log.log(data.error + ' ' + status);
                });
        };
    };

    tecajController.$inject = ['$scope', 'store', 'auth', '$log', 'trgovinaFactory'];

    angular.module('trgApp').controller('tecajController', tecajController);

} ());