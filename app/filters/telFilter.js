(function () {

    var telFilter = function () {
        return function (tel) {
            if (!tel) { return ''; }

            var value = tel.toString().trim().replace(/^\+/, '');

            if (value.match(/[^0-9]/)) {
                return tel;
            }

            var pozivni = value.slice(0, 3);
            var predBroj = value.slice(3, 5);
            var broj = value.slice(5);

            broj = broj.slice(0, 3) + '-' + broj.slice(3);

            return ('+ (' + pozivni + ')' + predBroj + '/' + broj).trim();
        };
    };

    angular.module('trgApp').filter('telFilter', telFilter);

} ());