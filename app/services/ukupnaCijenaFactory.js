(function () {
    'use strict';

    angular
        .module('trgApp')
        .factory('ukupnaCijena', ukupnaCijena);

    /** @ngInject */
    function ukupnaCijena() {

        return {
            racun: racun
        };

        function racun(cijena, kolicina) {
            return cijena * kolicina;
        }
    }

} ());