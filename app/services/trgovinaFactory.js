(function () {

    var trgovinaFactory = function ($http, $cacheFactory) {

        var factory = {};

        function deleteCacheKlijenti() {
            var httpCache = $cacheFactory.get('$http');
            httpCache.remove('/api/klijent');
        }

        function deleteCacheKlijent(klijentId) {
            var httpCache = $cacheFactory.get('$http');
            httpCache.remove('/api/klijent/' + klijentId);
        }

        function deleteCacheKupovine() {
            var httpCache = $cacheFactory.get('$http');
            httpCache.remove('/api/kupovine');
        }

        factory.getKlijenti = function () {
            return $http.get('/api/klijent', { cache: true });
        };

        factory.postKlijent = function (klijent) {
            deleteCacheKlijenti();
            return $http.post('/api/klijent', klijent);
        };

        factory.findIme = function (klijent) {
            return $http.get('/api/klijent/ime/' + klijent.ime);
        };

        factory.findPrezime = function (klijent) {
            return $http.get('/api/klijent/prezime/' + klijent.prezime);
        };

        factory.findBroj = function (klijent) {
            return $http.get('/api/klijent/broj/' + klijent.kontaktBroj);
        };

        factory.findAdresa = function (klijent) {
            return $http.get('/api/klijent/adresa/' + klijent.adresa);
        };

        factory.getAllKupovine = function () {
            return $http.get('/api/kupovine', { cache: true });
        };

        factory.getKupovine = function (klijentId) {
            return $http.get('/api/kupovine/' + klijentId);
        };

        factory.getKupovinu = function (klijentId) {
            return $http.get('/api/kupovina/' + klijentId);
        };

        factory.postKupovine = function (klijentId, kupovina) {
            deleteCacheKupovine();
            return $http.post('/api/kupovine/' + klijentId, kupovina);
        };

        factory.putKupovina = function (kupovinaId, kupovina) {
            deleteCacheKupovine();
            return $http.put('/api/kupovina/' + kupovinaId, kupovina);
        };

        factory.deleteKupovina = function (kupovinaId) {
            deleteCacheKupovine();
            return $http.delete('/api/kupovina/' + kupovinaId);
        };

        factory.getKlijent = function (klijentId) {
            return $http.get('/api/klijent/' + klijentId, { cache: true });
        };

        factory.putKlijent = function (klijentId, klijent) {
            deleteCacheKlijenti();
            deleteCacheKlijent(klijentId);
            return $http.put('/api/klijent/' + klijentId, klijent);
        };

        factory.deleteKlijent = function (klijentId) {
            deleteCacheKlijenti();
            deleteCacheKupovine();
            deleteCacheKlijent(klijentId);
            return $http.delete('/api/klijent/' + klijentId);
        };

        factory.getTecaj = function () {
            return $http.get('https://api.fixer.io/latest', {
                skipAuthorization: true
            });
        };

        // factory.getConvert = function (para1, para2, para3) {
        //     return $http.get('/api/convert/' + para1 + '/' + para2 + '/' + para3);
        // };

        factory.getConvert = function (para1, para3) {
            return $http.get('/api/convert/' + para1 + '/' + para3);
        };

        return factory;

    };

    trgovinaFactory.$inject = ['$http', '$cacheFactory'];

    angular.module('trgApp').factory('trgovinaFactory', trgovinaFactory);

} ());