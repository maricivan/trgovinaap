'use strict';

(function () {

    var app = angular.module('trgApp', ['auth0', 'angular-storage', 'angular-jwt', 'ui.mask', 'ngRoute', 'ngAnimate', 'chart.js']);

    app.config(function ($provide, $routeProvider, authProvider, $httpProvider, $locationProvider, jwtInterceptorProvider) {

        authProvider.init({
            domain: 'maricivan.eu.auth0.com',
            clientID: 'jrmjRLzNMoz6DuO52waXtQtg5ACCDMoR',
            callbackUrl: location.href
        });

        jwtInterceptorProvider.tokenGetter = function (store) {
            return store.get('id_token');
        };

        $routeProvider
            .when('/', {
                controller: 'klijentiController',
                templateUrl: 'views/klijenti.html',
                requiresLogin: true
            })
            .when('/kupovine/:klijentId', {
                controller: 'kupovineController',
                templateUrl: 'views/kupovine.html',
                requiresLogin: true
            })
            .when('/novi_klijent', {
                controller: 'noviklijentController',
                templateUrl: 'views/noviKlijent.html',
                requiresLogin: true
            })
            .when('/pretraga', {
                controller: 'pretragaController',
                templateUrl: 'views/pretraga.html',
                requiresLogin: true
            })
            .when('/uredi/:klijentId', {
                controller: 'urediKlijentController',
                templateUrl: 'views/uredi.html',
                requiresLogin: true
            })
            .when('/uredikupovinu/:kupovinaId', {
                controller: 'kupovineController',
                templateUrl: 'views/uredikupovinu.html',
                requiresLogin: true
            })
            .when('/tecaj', {
                controller: 'tecajController',
                templateUrl: 'views/tecaj.html',
                requiresLogin: true
            })
            .when('/login', {
                controller: 'authController',
                templateUrl: 'views/login.html'
            })
            .when('/kupovine', {
                controller: 'kupovineAllController',
                templateUrl: 'views/kupovineAll.html'
            })
            .otherwise({
                redirectTo: '/'
            });

        //Called when login is successful
        authProvider.on('loginSuccess', function ($location, profilePromise, idToken, store) {
            console.log('Login Success');
            profilePromise.then(function (profile) {
                store.set('profile', profile);
                store.set('token', idToken);
            });
            $location.path('/');
        });

        //Called when login fails
        authProvider.on('loginFailure', function ($location) {
            console.log('Error logging in');
            $location.path('/login');
        });

        //Angular HTTP Interceptor function
        jwtInterceptorProvider.tokenGetter = function (store) {
            return store.get('token');
        };
        //Push interceptor function to $httpProvider's interceptors
        $httpProvider.interceptors.push('jwtInterceptor');

        // function redirect($q, $injector, auth, store, $location) {
        //     return {
        //         responseError: function (rejection) {
        //             if (rejection.status === 401) {
        //                 auth.signout();
        //                 store.remove('profile');
        //                 store.remove('id_token');
        //                 $location.path('/');
        //             }

        //             return $q.reject(rejection);
        //         }
        //     };
        // }

        // $provide.factory('redirect', redirect);
        // $httpProvider.interceptors.push('redirect');
        // $httpProvider.interceptors.push('jwtInterceptor');

    });

    app.run(function ($rootScope, auth, store, jwtHelper, $location) {

        $rootScope.$on('$locationChangeStart', function () {

            var token = store.get('id_token');
            if (token) {
                if (!jwtHelper.isTokenExpired(token)) {
                    if (!auth.isAuthenticated) {
                        auth.authenticate(store.get('profile'), token);
                    }
                }
            } else {
                $location.path('/login');
            }
        });
        // This hooks all auth events to check everything as soon as the app starts
        auth.hookEvents();
    });

} ());