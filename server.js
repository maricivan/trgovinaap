var express = require('express');
var app = express();
var http = require('http');
var path = require('path');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var sequelize = require('sequelize');
var unirest = require('unirest');
var soap = require('soap');
var jwt = require('express-jwt');

var port = process.env.PORT || 5000;

var models = require('./models');
var Klijent = models.Klijent;
var Kupovina = models.Kupovina;
app.set('models', require('./models'));
var Klijent = app.get('models').Klijent;
var Kupovina = app.get('models').Kupovina;

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var authCheck = jwt({
    secret: new Buffer('Tf1ee5nmGQYDlvi3GP2nHg3v_iFpD5boc0C0kbi8v8KlTpi4udexotCL8fpeSDRR', 'base64'),
    audience: 'jrmjRLzNMoz6DuO52waXtQtg5ACCDMoR'
});

app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'app')));

app.get('/', function (req, res) {
    res.render('index', {title: 'Express'});
});

//Get Klijenti
app.get('/api/klijent', authCheck, function (req, res) {
    Klijent.findAll().then(function (klijenti) {
        res.json(klijenti);
    }).error(function (err) {
        console.log('Problem pri dohvatu klijenata.');
    });
});

//Get Klijent
app.get('/api/klijent/:_id', authCheck, function (req, res) {
    var idKlijenta = req.params._id;
    Klijent.findById(idKlijenta).then(function (klijent) {
        res.json(klijent);
    }).error(function (err) {
        console.log('Problem pri dohvatu podataka.');
    });
});

// //Get Kupovina
// app.get('/api/kupovina/:_id', authCheck, function (req, res) {
//     var idKlijenta = req.params._id;
//     Kupovina.findAll({where: {KlijentId: idKlijenta}}).then(function (kupovina) {
//         res.json(kupovina);
//     }).error(function (err) {
//         console.log('Problem pri dohvatu klijenata.');
//     });
// });

//Post Klijent
app.post('/api/klijent', authCheck, function (req, res) {
    var klijent = req.body;
    Klijent.create(klijent).then(function (klijenti) {
        res.json(klijenti);
    }).error(function (err) {
        console.log('Problem pri spremanju podataka.');
    });
});

//Update Klijent
app.put('/api/klijent/:_id', authCheck, function (req, res) {
    var idKlijenta = req.params._id;
    Klijent.update((req.body), {where: {id: idKlijenta}}).then(function (klijenti) {
        res.json(klijenti);
    }).error(function (err) {
        console.log('Problem pri spremanju podataka.');
    });
});

//Delete Klijent
app.delete('/api/klijent/:_id', authCheck, function (req, res) {
    var idKlijenta = req.params._id;
    Klijent.destroy({where: {id: idKlijenta}}).then(function (klijenti) {
        res.json(klijenti);
    }).error(function (err) {
        console.log('Problem pri brisanju podataka.');
    });
});

//Trazi ime
app.get('/api/klijent/ime/:ime', authCheck, function (req, res) {
    var ime = req.params.ime;
    Klijent.findAll({where: ['ime like ?', '%' + ime + '%']}).then(function (klijenti) {
        res.json(klijenti);
    });
});

//Trazi prezime
app.get('/api/klijent/prezime/:prezime', authCheck, function (req, res) {
    var prezime = req.params.prezime;
    Klijent.findAll({where: ['prezime like ?', '%' + prezime + '%']}).then(function (klijenti) {
        res.json(klijenti);
    });
});

//Trazi broj
app.get('/api/klijent/broj/:broj', authCheck, function (req, res) {
    var broj = req.params.broj;
    Klijent.findAll({where: ['kontaktBroj like ?', '%' + broj + '%']}).then(function (klijenti) {
        res.json(klijenti);
    });
});

//Trazi adresa
app.get('/api/klijent/adresa/:adresa', authCheck, function (req, res) {
    var adresa = req.params.adresa;
    Klijent.findAll({where: ['adresa like ?', '%' + adresa + '%']}).then(function (klijenti) {
        res.json(klijenti);
    });
});

//Get All Kupovina
app.get('/api/kupovine', authCheck, function (req, res) {
    Kupovina.findAll().then(function (kupovine) {
        res.json(kupovine);
    }).error(function (err) {
        console.log('Problem pri dohvatu kupovina.');
    });
});

//Get Kupovine
app.get('/api/kupovine/:_id', authCheck, function(req, res) {
    var id = req.params._id;
    Kupovina.findAll({where: {KlijentId: id}}).then(function(kupovine) {
        res.json(kupovine);
    }).error(function(err) {
        console.log('Problem pri dohvatu kupovina.');
    });
});

//Get Kupovina
app.get('/api/kupovina/:_id', authCheck, function (req, res) {
    var idKupovine = req.params._id;
    Kupovina.findById(idKupovine).then(function (kupovina) {
        res.json(kupovina);
    }).error(function (err) {
        console.log('Problem pri dohvatu kupovine.');
    });
});

//Post Kupovine
app.post('/api/kupovine/:_id', authCheck, function(req, res) {
    var id = req.params._id;
    Kupovina.create({
        proizvod: req.body.proizvod,
        kolicina: req.body.kolicina,
        cijena: req.body.cijena,
        ukupno: req.body.ukupno,
        napomena: req.body.napomena,
        KlijentId: id
    }).then(function(klijenti) {
        res.json(klijenti);
    }).error(function(err) {
        console.log('Problem pri spremanju kupovina.');
    });
});

//Update Kupovina
app.put('/api/kupovina/:_id', authCheck, function (req, res) {
    var idKupovine = req.params._id;
    Kupovina.update((req.body), {where: {id: idKupovine}}).then(function (kupovina) {
        res.json(kupovina);
    }).error(function (err) {
        console.log('Problem pri spremanju podataka.');
    });
});

//Delete Kupovina
app.delete('/api/kupovina/:_id', authCheck, function (req, res) {
    var idKupovine = req.params._id;
    Kupovina.destroy({where: {id: idKupovine}}).then(function (kupovine) {
        res.json(kupovine);
    }).error(function (err) {
        console.log('Problem pri brisanju podataka.');
    });
});

//Get Tečaj
app.get('https://api.fixer.io/latest', function (req, res) {
    var data = res.body;
    res.json(data);
});

//Get Convert
// app.get('/api/convert/:para1/:para2/:para3', authCheck, function (req, res) {
//     var para1 = req.params.para1;
//     var para2 = req.params.para2;
//     var para3 = req.params.para3;
//     unirest.get('https://currencyconverter.p.mashape.com/?from=' + para1 + '&from_amount=' + para2 + '&to=' + para3)
//         .header('X-Mashape-Key', 'nJ0C8qy2HAmshBUgXIR9uIXSCxrIp1zzhfPjsnl6unMIpHfZgb')
//         .header('Accept', 'application/json')
//         .end(function (result) {
//             res.json(result.body);
//         });
// });

var url = 'http://www.webservicex.net/CurrencyConvertor.asmx?WSDL';

app.get('/api/convert/:para1/:para3', authCheck, function (req, res) {
    var para1 = req.params.para1;
    var para3 = req.params.para3;
    var args = {
        FromCurrency: para1,
        ToCurrency: para3
    };
    soap.createClient(url, function (err, client) {
        client.ConversionRate(args, function (err, result) {
            console.log(result);
            res.json(result);
            res.end();
        });
    });
});

app.listen(port, function (err) {
    console.log('Server pokrenut na portu ' + port);
});